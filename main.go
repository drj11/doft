package main

import (
	"flag"
	"fmt"
	"log"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"
)
import (
	"gitlab.com/font8/coff"
	"gitlab.com/font8/opentype"
	"gitlab.com/font8/way"
)

var version string

var versionp = flag.Bool("version", false, "Show version of tool")
var glyfp = flag.String("glyf", "∀", "list of glyphs by numeric index (comma separated)")
var glyphp = flag.String("glyph", "", "list of glyphs by name (comma separated)")

var gridp = flag.String("grid", "72,36", "width and height of output grid")
var bboxp = flag.String("bbox", "0,-200,1000,1000", "x,y bounds (bottom-left, top-right)")

var debugcontp = flag.Bool("debugcont", false, "Show contour debugging")

func main() {
	flag.Parse()

	if *versionp {
		fmt.Println(version)
		return
	}

	name := flag.Arg(0)
	r, err := os.Open(name)
	if err != nil {
		log.Fatal(err)
	}
	font, err := opentype.OpenFont(r)
	if err != nil {
		log.Fatal(err)
	}

	// Collect all the glyphs we are interested in,
	// by their index.
	enumerated := []int{}

	N, err := font.GlyphCount()
	if err != nil {
		log.Fatal(err)
	}

	if *glyphp != "" {
		font_names, err := font.GlyphNames()
		if err != nil {
			log.Fatal(err)
		}
		names := strings.Split(*glyphp, ",")
		for _, name := range names {
			name = strings.TrimSpace(name)
			// :todo: not be quadratic
			for i := range font_names {
				if name == font_names[i] {
					enumerated =
						append(enumerated, i)
					break
				}
			}
		}
	} else if *glyfp == "∀" {
		if err != nil {
			log.Fatal(err)
		}
		for i := 0; i < N; i += 1 {
			enumerated = append(enumerated, i)
		}
	} else {
		for _, glyf := range strings.Split(*glyfp, ",") {
			i, err := strconv.Atoi(glyf)
			if err != nil {
				log.Print(err)
				continue
			}
			enumerated = append(enumerated, i)
		}
	}

	// x and y offset for placing the next glyph
	// cx := 0.0
	// cy := 0.0

	for _, i := range enumerated {
		glyph := font.FetchGlyph(i)

		o := glyph.ToOutline()
		ScanO(o)
	}
}

func ScanO(o *coff.Outline) {
	// Béziers
	bs := []way.B{}

	for i, contour := range o.Contours {
		ps := contour.P
		if len(ps) <= 0 {
			log.Fatalf("Mysteriously, an empty contour %v in outline %v",
				i, o)
		}
		N := len(ps)
		if !ps[0].OnCurve {
			log.Fatalf("First point of contour %v is not OnCurve %v",
				i, contour)
		}
		if !ps[N-1].OnCurve {
			log.Fatalf("Last point of contour %v is not OnCurve %v",
				i, contour)
		}

		// go through and make Bézier curves;
		// for all but the first point (which is a
		// starting point), every onCurve point is the
		// end of a Bézier curve.
		b := way.B{}
		for j := 0; j < N; j++ {
			b.P = append(b.P, way.Point{ps[j].X, ps[j].Y})
			if j > 0 && ps[j].OnCurve {
				bs = append(bs, b)
				b = way.B{}
				b.P = append(b.P, way.Point{ps[j].X, ps[j].Y})
			}
		}
	}
	if *debugcontp {
		for i := range bs {
			fmt.Println(i, bs[i])
		}
	}

	grid := []int{}
	for _, s := range strings.Split(*gridp, ",") {
		i, _ := strconv.ParseInt(strings.TrimSpace(s), 10, 64)
		grid = append(grid, int(i))
	}
	COLS := grid[0]
	ROWS := grid[1]

	bbox := []float64{}
	for _, s := range strings.Split(*bboxp, ",") {
		f, _ := strconv.ParseFloat(strings.TrimSpace(s), 64)
		bbox = append(bbox, f)
	}

	for row := 0; row < ROWS; row++ {
		// We scan from top to bottom, but in terms
		// of the usual OpenType grid, that's from high Y
		// to low Y
		ny := (float64(ROWS-row) - 0.5) / float64(ROWS)
		at := ny*(bbox[3]-bbox[1]) + bbox[1]

		// all the hits (in this row) for all contours
		hits := []way.Point{}
		hit_contours := []int{}
		// hit all contours, then sort the combined list
		for i, bez := range bs {
			ps, err := bez.HorizontalHit(at)
			if err != nil {
				log.Fatal(err)
			}
			hits = append(hits, ps...)
			if len(ps) > 0 {
				hit_contours = append(hit_contours, i)
			}
		}

		// A fake hit at infinity makes sure the comparison
		// in the loop can't run dry
		hits = append(hits, way.Point{math.Inf(1), at})
		sort.Slice(hits, func(i, j int) bool {
			return hits[i].X < hits[j].X
		})

		// each scanline starts outside
		in := false
		for col := 0; col < COLS; col++ {
			// sample at centre of pixel
			nx := (float64(col) + 0.5) / float64(COLS)
			x := nx*(bbox[2]-bbox[0]) + bbox[0]
			for hits[0].X <= x {
				in = !in
				hits = hits[1:]
			}
			cell := "·"
			if in {
				cell = "■"
			}
			fmt.Print(cell)
		}
		if *debugcontp {
			fmt.Print(at, hit_contours)
		}
		fmt.Println()
	}
}
