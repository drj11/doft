module doft

// replace gitlab.com/font8/way => ../way
// replace gitlab.com/font8/opentype => ../opentype
// replace gitlab.com/font8/coff => ../coff

go 1.16

require (
	gitlab.com/font8/coff v0.0.0-20221029183915-55542b7510e9
	gitlab.com/font8/opentype v0.0.0-20221029184807-c3a962d2b95d
	gitlab.com/font8/way v0.0.0-20221029150633-143071ea4631
)
