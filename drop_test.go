package main

// A demonstration of scanline conversion of cubic Bézier curves

import (
	"fmt"
	"gitlab.com/font8/way"
	"log"
	"math"
)

import (
	"testing"
)

func TestDrop(t *testing.T) {
	// points on a notional 1000×1000 grid
	UPM := 1000.0
	b := way.B{[]way.Point{{375, 250}, {0, 1000}, {1000, 1000}, {375, 250}}}

	ROWS := 40
	COLS := 80

	for row := 0; row < ROWS; row++ {
		// We scan from top to bottom, but in terms
		// of the usual OpenType grid, that's from high Y
		// to low Y
		at := UPM * (float64(ROWS-row) - 0.5) / float64(ROWS)
		ps, err := b.HorizontalHit(at)
		if err != nil {
			log.Fatal(err)
		}

		// A fake hit at infinity makes sure the comparison
		// in the loop can't run dry
		ps = append(ps, way.Point{math.Inf(1), at})

		// each scanline starts outside
		in := false
		for col := 0; col < COLS; col++ {
			// sample at centre of pixel
			x := UPM * (float64(col) + 0.5) /
				float64(COLS)
			for ps[0].X <= x {
				in = !in
				ps = ps[1:]
			}
			cell := "·"
			if in {
				cell = "■"
			}
			fmt.Print(cell)
		}
		fmt.Println()
	}
}
