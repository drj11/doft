# doft—Dot from font

The job of these softwares is to transform from a continuous
outline representation to a discrete area representation.

The classic target would be a square pixel grid, but
the reason _this_ software exists is to experiment with the grid.

Core to this mission is the idea of "evaluating" a closed curve
at a point, primarily to determine if the point is inside or
outside the closed curve.

One possible product would be a bi-level rasteriser.

There is a demo of this idea in the test in `drop_test.go`.


# END
